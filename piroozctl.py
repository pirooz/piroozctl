#!/usr/bin/python3

"""
    Piroozctl - Command-Line Interface for V2Ray Servers
    Copyright (C) 2022  Pirooz Git <pirooz_git@riseup.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
# Tested on Debian 11
# $ sudo python3 piroozctl.py --install-omnibus --single-server --domain sub.example.com
# TODO: add cli commands: --game; --protocol ; --scenario 1, 2, 3, ...

import argparse
import base64
import subprocess
import uuid
from pathlib import Path
from random import randrange
from string import Template

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--install-omnibus',
                    action='store_true',
                    dest='install_omnibus',
                    help='Installing Omnibus Pirooczctl'
                    )

parser.add_argument('-s', '--single-server',
                    action='store_true',
                    dest='single_server',
                    help='Installing Omnibus Pirooczctl on a single server'
                    )

parser.add_argument('-d', '--domain',
                    dest='domain',
                    help='Domain name'
                    )


args = parser.parse_args()

if args.install_omnibus is True:
    if args.single_server is True:

        # update and upgrade the GNU/Linux
        subprocess.call('sudo apt update', shell=True)

        # install v2ray from Debian repos
        subprocess.call('sudo apt install -y v2ray', shell=True)

        # configure v2ray
        subprocess.call('wget https://codeberg.org/pirooz/piroozctl/raw/branch/main/config.json.pytemplate', shell=True)
        with open('config.json.pytemplate') as f:
            v2ray_config_template = Template(f.read())
        
        INBOUNDS_PORT = randrange(49152, 65535)
        CLIENTS_ID = uuid.uuid4()
        WS_PATH = uuid.uuid4().hex
        v2ray_config = v2ray_config_template.substitute(CLIENTS_ID=CLIENTS_ID, WS_PATH=WS_PATH,
                                                        INBOUNDS_PORT=INBOUNDS_PORT)

        with open("/etc/v2ray/config.json", "w") as f:
            f.write(v2ray_config)

        subprocess.call('sudo systemctl restart v2ray', shell=True)

        # install and configure nginx
        subprocess.call('sudo apt install -y nginx', shell=True)

        nginx_server_block_template = Template("""
        server {
            listen 80;
            server_name    $SERVER_NAME;
            
            index index.html;
            root /usr/share/nginx/html/;
        
            access_log /var/log/nginx/v2ray.access;
            error_log /var/log/nginx/v2ray.error;

            location /$LOCATION { # Consistent with the path of V2Ray configuration
                if ($$http_upgrade != "websocket") { # Return 404 error when WebSocket upgrading negotiate failed
                    return 404;
                }
                proxy_redirect off;
                proxy_pass http://127.0.0.1:$WS_PORT; # websocket port
                proxy_http_version 1.1;
                proxy_set_header Upgrade $$http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_set_header Host $$host;
                # Show real IP in v2ray access.log
                proxy_set_header X-Real-IP $$remote_addr;
                proxy_set_header X-Forwarded-For $$proxy_add_x_forwarded_for;
            }
        }
        """)
        nginx_server_block = nginx_server_block_template.substitute(SERVER_NAME=args.domain, LOCATION=WS_PATH, WS_PORT=INBOUNDS_PORT)

        with open("/etc/nginx/conf.d/v2ray.conf", "w") as f:
            f.write(nginx_server_block)

        subprocess.call('sudo systemctl restart nginx', shell=True)

        # install and configure certbot
        subprocess.call('sudo apt install -y certbot python3-certbot-nginx', shell=True)
        subprocess.call(f'sudo certbot --webroot -i nginx --agree-tos --hsts --staple-ocsp  --agree-tos '
                        f'--register-unsafely-without-email -d {args.domain} -w /usr/share/nginx/html/', shell=True)
        subprocess.call('sudo systemctl restart nginx', shell=True)

        # enable TCP BBR
        with open("/etc/sysctl.d/60-custom.conf", "a") as f:
            f.write('\n')
            f.write('net.core.default_qdisc=fq')
            f.write('\n')
            f.write('net.ipv4.tcp_congestion_control=bbr')
            f.write('\n')

        subprocess.call('sudo sysctl -p /etc/sysctl.d/60-custom.conf', shell=True)

        # install and configure fail2ban
        subprocess.call('sudo apt install -y fail2ban', shell=True)
        subprocess.call('sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local', shell=True)
        subprocess.call('sudo systemctl restart fail2ban', shell=True)

        # install and configure UFW
        subprocess.call('sudo apt install -y ufw', shell=True)
        subprocess.call('sudo ufw allow 443/tcp', shell=True)
        subprocess.call('sudo ufw allow 22/tcp', shell=True)
        subprocess.call('sudo ufw --force enable', shell=True)
        
        # create VMess URL
        vmess_config_string_template = Template("""
        {
            "v": "2",
            "ps": "$DOMAIN",
            "add": "$DOMAIN",
            "port": "443",
            "id": "$ID",
            "aid": "0",
            "net": "ws",
            "type": "none",
            "path": "/$PATH",
            "tls": "tls"
        }
        """)
        vmess_config_string = vmess_config_string_template.substitute(DOMAIN=args.domain, ID=CLIENTS_ID, PATH=WS_PATH)
        vmess_config_string_bytes = base64.b64encode(vmess_config_string.encode("ascii"))
        vmess_link = str(vmess_config_string_bytes, "ascii")
        vmess_link = 'vmess://' + vmess_link

        with open("vmess_link", "w") as f:
            f.write(vmess_link)
